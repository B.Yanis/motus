﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Motus
{
    public partial class Form1 : Form
    {
        string[] tabCache;
        int count;
        public Form1()
        {
            InitializeComponent();
            cbxNbLettres.Items.Add(8);
            cbxNbLettres.Items.Add(9);
            tbxProposition.MaxLength = 8;

            tbxProposition.CharacterCasing = CharacterCasing.Upper;
           

            //limiter la taille du mot
            //tbxProposition.MaxLength = Convert.ToByte(cbxNbLettres.Text);

        }

        private void tbxProposition_KeyDown(object sender, KeyEventArgs e)
        {
            {
                if (((int)e.KeyCode >= 1 && (int)e.KeyCode <= 7)
                || ((int)e.KeyCode >= 9 && (int)e.KeyCode <= 36)
                || ((int)e.KeyCode >= 40 && (int)e.KeyCode <= 45)
                || ((int)e.KeyCode >= 47 && (int)e.KeyCode <= 64)
                || ((int)e.KeyCode == 38)
                || ((int)e.KeyCode > 90)
                )
                {
                    e.SuppressKeyPress = true;
                }
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {

            dgvMotus.Width = 300;


        }

        public void bntNouvellePartie_Click(object sender, EventArgs e)
        {
            dgvMotus.ColumnCount = Convert.ToInt32(cbxNbLettres.Text);
            dgvMotus.ReadOnly = true;
            dgvMotus.RowCount = 7;
            string motCache;
            Random tirage = new Random();
            string path = "..\\..\\..\\..\\";
            string nomFichier = "mots" + cbxNbLettres.SelectedItem + ".txt";
            path = path + nomFichier;

            int nbMots = File.ReadAllLines(path).Count();

            String[] lesLignes = File.ReadAllLines(path);

            int positionMot = tirage.Next(0, nbMots);

            motCache = lesLignes[positionMot];
            Console.WriteLine(motCache);
            count = 0;
            string motCacheSep = "";
            for(int i = 0; i < motCache.Length; i++)
            {
                if (i == motCache.Length - 1)
                {
                    motCacheSep += motCache[i];
                }
                else
                {
                    motCacheSep += motCache[i] + ",";
                }
                
                
            }
            tabCache = motCacheSep.Split(',');
        }

        private void bntValider_Click(object sender, EventArgs e)
        {
            string[] resultDgv = new string[8];
            string proposition = tbxProposition.Text;
            Console.WriteLine(tbxProposition.Text);
            Console.WriteLine();
            string[] tab;
            string propositionSep = "";
            Regex min = new Regex(@"^([\p{L}]{8})$");
            if (!min.IsMatch(tbxProposition.Text))
            {
                tbxProposition.Text = null;
                MessageBox.Show("Le mot doit posséder minimum 8 caractères");
            }
            else
            {
                for (int i = 0; i < proposition.Length; i++)
                {
                    if (i == proposition.Length - 1)
                    {
                        propositionSep += proposition[i];

                    }

                    else
                    {
                        propositionSep += proposition[i] + ",";

                    }

                }
                tab = propositionSep.Split(',');

                if (count < 6)
                {
                    
                    for (int j = 0; j < tabCache.Length; j++)
                    {
                        if (tabCache[j] == tab[j])
                        {

                            dgvMotus[j, count].Value = tab[j];
                            dgvMotus[j, count].Style.BackColor = Color.Green;
                            
                        }
                        else if (tabCache.Contains(tab[j]))
                        {
                            dgvMotus[j, count].Value = tab[j];
                            dgvMotus[j, count].Style.BackColor = Color.Orange;
                            

                        }
                        else
                        {
                            dgvMotus[j, count].Value = tab[j];
                            dgvMotus[j, count].Style.BackColor = Color.Red;
                            
                        }

                        resultDgv[j] = dgvMotus[j, count].Value.ToString();

                    }
                    count++;

                }
                else if (count == 6)
                {
                    if (tabCache.ToString() == resultDgv.ToString())
                    {
                        MessageBox.Show("Vous avez Gagner !!!");
                    }
                    else
                    {
                        MessageBox.Show("Perdu");
                    }
                }
                else
                {
                    MessageBox.Show("Perdu");
                }
            }




            
            

        }
    }
}
