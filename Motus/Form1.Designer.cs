﻿
namespace Motus
{
    partial class Form1
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tbxProposition = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.cbxNbLettres = new System.Windows.Forms.ComboBox();
            this.bntValider = new System.Windows.Forms.Button();
            this.bntNouvellePartie = new System.Windows.Forms.Button();
            this.bntAnnuler = new System.Windows.Forms.Button();
            this.bntQuitter = new System.Windows.Forms.Button();
            this.dgvMotus = new System.Windows.Forms.DataGridView();
            ((System.ComponentModel.ISupportInitialize)(this.dgvMotus)).BeginInit();
            this.SuspendLayout();
            // 
            // tbxProposition
            // 
            this.tbxProposition.AllowDrop = true;
            this.tbxProposition.Location = new System.Drawing.Point(476, 104);
            this.tbxProposition.Name = "tbxProposition";
            this.tbxProposition.Size = new System.Drawing.Size(183, 23);
            this.tbxProposition.TabIndex = 0;
            this.tbxProposition.KeyDown += new System.Windows.Forms.KeyEventHandler(this.tbxProposition_KeyDown);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(239, 40);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(200, 15);
            this.label1.TabIndex = 1;
            this.label1.Text = "Choisir le nombre de lettres du mot :";
            // 
            // cbxNbLettres
            // 
            this.cbxNbLettres.AllowDrop = true;
            this.cbxNbLettres.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbxNbLettres.Location = new System.Drawing.Point(476, 37);
            this.cbxNbLettres.Name = "cbxNbLettres";
            this.cbxNbLettres.Size = new System.Drawing.Size(152, 23);
            this.cbxNbLettres.TabIndex = 3;
            // 
            // bntValider
            // 
            this.bntValider.Location = new System.Drawing.Point(831, 90);
            this.bntValider.Name = "bntValider";
            this.bntValider.Size = new System.Drawing.Size(93, 48);
            this.bntValider.TabIndex = 4;
            this.bntValider.Text = "Valider";
            this.bntValider.UseVisualStyleBackColor = true;
            this.bntValider.Click += new System.EventHandler(this.bntValider_Click);
            // 
            // bntNouvellePartie
            // 
            this.bntNouvellePartie.Location = new System.Drawing.Point(447, 458);
            this.bntNouvellePartie.Name = "bntNouvellePartie";
            this.bntNouvellePartie.Size = new System.Drawing.Size(99, 48);
            this.bntNouvellePartie.TabIndex = 5;
            this.bntNouvellePartie.Text = "Nouvelle Partie";
            this.bntNouvellePartie.UseVisualStyleBackColor = true;
            this.bntNouvellePartie.Click += new System.EventHandler(this.bntNouvellePartie_Click);
            // 
            // bntAnnuler
            // 
            this.bntAnnuler.Location = new System.Drawing.Point(603, 458);
            this.bntAnnuler.Name = "bntAnnuler";
            this.bntAnnuler.Size = new System.Drawing.Size(99, 48);
            this.bntAnnuler.TabIndex = 6;
            this.bntAnnuler.Text = "Annuler";
            this.bntAnnuler.UseVisualStyleBackColor = true;
            // 
            // bntQuitter
            // 
            this.bntQuitter.Location = new System.Drawing.Point(759, 458);
            this.bntQuitter.Name = "bntQuitter";
            this.bntQuitter.Size = new System.Drawing.Size(99, 48);
            this.bntQuitter.TabIndex = 8;
            this.bntQuitter.Text = "Quitter";
            this.bntQuitter.UseVisualStyleBackColor = true;
            // 
            // dgvMotus
            // 
            this.dgvMotus.AllowUserToAddRows = false;
            this.dgvMotus.AllowUserToDeleteRows = false;
            this.dgvMotus.AllowUserToResizeColumns = false;
            this.dgvMotus.AllowUserToResizeRows = false;
            this.dgvMotus.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvMotus.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dgvMotus.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            this.dgvMotus.ColumnHeadersVisible = false;
            this.dgvMotus.Location = new System.Drawing.Point(239, 155);
            this.dgvMotus.Margin = new System.Windows.Forms.Padding(0);
            this.dgvMotus.MultiSelect = false;
            this.dgvMotus.Name = "dgvMotus";
            this.dgvMotus.ReadOnly = true;
            this.dgvMotus.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.dgvMotus.RowHeadersVisible = false;
            this.dgvMotus.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.dgvMotus.RowTemplate.Height = 25;
            this.dgvMotus.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect;
            this.dgvMotus.ShowEditingIcon = false;
            this.dgvMotus.Size = new System.Drawing.Size(375, 300);
            this.dgvMotus.TabIndex = 9;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1225, 556);
            this.Controls.Add(this.dgvMotus);
            this.Controls.Add(this.bntQuitter);
            this.Controls.Add(this.bntAnnuler);
            this.Controls.Add(this.bntNouvellePartie);
            this.Controls.Add(this.bntValider);
            this.Controls.Add(this.cbxNbLettres);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.tbxProposition);
            this.Name = "Form1";
            this.Text = "Motus";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgvMotus)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        public System.Windows.Forms.TextBox tbxProposition;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox cbxNbLettres;
        private System.Windows.Forms.Button bntValider;
        private System.Windows.Forms.Button bntNouvellePartie;
        private System.Windows.Forms.Button bntAnnuler;
        private System.Windows.Forms.Button bntQuitter;
        private System.Windows.Forms.DataGridView dgvMotus;
    }
}

